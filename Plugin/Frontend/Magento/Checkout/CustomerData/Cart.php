<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\CartDiscountAmountInMinicart\Plugin\Frontend\Magento\Checkout\CustomerData;

class Cart
{

    protected $checkoutSession;
    protected $cartTotalRepository;

    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Quote\Model\Cart\CartTotalRepository $cartTotalRepository,
        \Magento\Framework\Pricing\Helper\Data $priceHelper
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->cartTotalRepository = $cartTotalRepository;
        $this->priceHelper = $priceHelper;
    }


    public function aroundGetSectionData(
        \Magento\Checkout\CustomerData\Cart $subject,
        \Closure $proceed
    ) {
        $data = $proceed();
        $totals = $this->cartTotalRepository->get($this->checkoutSession->getQuote()->getId());

        $discountAmount = $totals->getDiscountAmount();
        $subtotal_excl_tax = $totals->getSubtotal();
        $subtotal_with_discount = $subtotal_excl_tax + $discountAmount;
        $display_discount_totals = ($subtotal_excl_tax == $subtotal_with_discount ) ? false : true;
        $atts=[
            'display_discount_totals' => $display_discount_totals,
            'cart_discount'=> $this->priceHelper->currency($discountAmount, true, false),
            'subtotal_with_discount'=> $this->priceHelper->currency($subtotal_with_discount, true, false)
        ];
        return array_merge($data, $atts);

    }
}

